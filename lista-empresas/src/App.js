import React from 'react'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Login from './empresa/Login'
import Home from './empresa/Home'
import './App.css'

export default () => {
  return (
    <Router>
      <div className="root-body">
        <Route path="/login" component={Login} />
        <PrivateRoute path="/home" component={Home} />
        <Route exact path="/" render={(() => <Redirect to="/login" />)} />
      </div>
    </Router>
  )
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      sessionStorage.getItem("access-token") ? (
        <Component {...props} />
      ) : (
        <Redirect
          to="/login"
        />
      )
    }
  />
);