import React from 'react'
import './Card.css'
import { baseImageUrl } from '../service/empresa'

export default (props) => {
  return (
    <div className="card card-flex" onClick={(e) => props.onClick(props.item.id, e)}>
      <div className="card-body">
        <img alt="logo" className="card-logo" src={`${baseImageUrl}${props.item.photo}`} onError={(e) => {e.target.src="https://blog.stylingandroid.com/wp-content/themes/lontano-pro/images/no-image-slide.png"}} />
        <div className="card-description">
          <h5 className="card-title">{props.item.enterprise_name}</h5>
          <p className="card-text">{props.item.enterprise_type.enterprise_type_name}</p>
          <p className="card-text">{props.item.country}</p>
        </div>
      </div>
    </div>
  )
}