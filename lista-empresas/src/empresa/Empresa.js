import React, { Component } from 'react'
import EmpresaService from '../service/empresa'
import { baseImageUrl } from '../service/empresa'
import './Empresa.css'

const Card = (props) => {
  return (
    <div className="card card-empresa mb-3">
      <img alt="Card" className="card-img-top" src={`${baseImageUrl}${props.empresa.photo}`} onError={(e) => {e.target.src="https://blog.stylingandroid.com/wp-content/themes/lontano-pro/images/no-image-slide.png"}} />
      <div className="card-body">
        <h5 className="card-title">{props.empresa.enterprise_name}</h5>
        <p className="card-text"><small className="text-muted">{props.empresa.description}</small></p>
      </div>
    </div>
  )
}
export default class Empresa extends Component {

  constructor() {
    super()
    this.state = {
      empresa: {}
    }
    this.homeButton = this.homeButton.bind(this)
  }

  componentDidMount() {
    EmpresaService.getEmpresaById(this.props.match.params.id)
      .then((response) => {
        this.setState({empresa: response})
      })
      .catch((response) => {
        console.log(response);
      })
  }

  homeButton() {
    this.props.history.push('/home/listagem')
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <Card empresa={this.state.empresa} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <button className="btn btn-secondary float-right" onClick={this.homeButton}>Voltar</button>
          </div>
        </div>
      </div>
    )
  }
}
