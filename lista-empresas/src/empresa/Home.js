import React, { Component } from 'react'
import './Home.css'
import Listagem from './Listagem'
import { Route } from 'react-router-dom'
import logo from './logo-nav.png'
import Empresa from './Empresa'

const HeadBar = (props) => {
  return(
    <nav className="navbar navbar-default justify-content-between">
      <div className="navbar-brand">
        <img alt="Home" src={logo} className="logo-head-bar" />
      </div>
      <button className="btn btn-secondary float-right" onClick={props.logout}>Sair</button>
    </nav>
  )
}

class Home extends Component {

  constructor(props) {
    super()
    this.state = {}
    this.logout = this.logout.bind(this)
  }

  logout() {
    this.props.history.push('/login')
  }

  render() {
    const baseUlr = this.props.match.url
    return (
      <div className="home-content">
        <HeadBar logout={this.logout}/>
        <Route path={`${baseUlr}/listagem`} component={Listagem} />
        <Route path={`${baseUlr}/empresa/:id`} component={Empresa} />
      </div>
    )
  }
}

export default Home
