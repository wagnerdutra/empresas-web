import React, { Component } from 'react'
import './Home.css'
import EmpresaService from '../service/empresa'
import Card from '../components/Card'
import _ from 'lodash'
import './Listagem.css'

class Listagem extends Component {

  constructor() {
    super()
    this.state = {
      empresaName: '',
      empresas: [],
      nome: '',
      tipo: ''
    }
    this.filterEmpresas = this.filterEmpresas.bind(this)
    this.getEmpresa = this.getEmpresa.bind(this)
    this.setUserFieldValue = this.setUserFieldValue.bind(this)
  }

  componentDidMount() {
    EmpresaService.getEmpresas()
      .then((response) => {
        this.setState({empresas: response.enterprises})
      })
      .catch((response) => {
        console.log(response);
      })
  }

  setUserFieldValue(e) {
    this.setState({
      [e.target.name]: e.target.value
    }, () => this.filterEmpresas({
      nome: this.state.nome,
      tipo: this.state.tipo
    }))
  }

  filterEmpresas(empresa) {
    _.debounce(() => EmpresaService.getEmpresasByFields(empresa)
      .then((response) => {
        console.log(response)
        this.setState({empresas: response.enterprises})
      })
      .catch((response) => {
        console.log(response);
      }), 700)()
  }

  getEmpresa(e) {
    this.props.history.push(`/home/empresa/${e}`)
  }

  render() {
    return (
      <div class="listagem-body">
        <div className="container">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputNome">Nome</label>
              <input type="text" className="form-control" id="inputNome" name="nome" value={this.state.nome} onChange={this.setUserFieldValue}/>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputTipo">Tipo</label>
              <select className="custom-select" id="inputGroupSelect01" name="tipo" value={this.state.tipo} onChange={this.setUserFieldValue}>
                <option value="21">Software</option>
                <option value="7">Fashion</option>
                <option value="18">Service</option>
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col">
              {this.state.empresas.map(empresa =>
                <Card key={empresa.id} item={empresa} onClick={this.getEmpresa}/>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Listagem
