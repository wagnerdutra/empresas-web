import React, { Component } from 'react'
import './Login.css'
import EmpresaService from '../service/empresa'
import logoLogin from './logo-home.png'

class Login extends Component {

  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
    }
    this.setUserFieldValue = this.setUserFieldValue.bind(this)
    this.login = this.login.bind(this)
  }

  setUserFieldValue(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  login(event) {
    event.preventDefault()

    EmpresaService.login({
      ...this.state
    }).then((response) => {
      sessionStorage.setItem("access-token", response.headers['access-token'])
      sessionStorage.setItem("client", response.headers['client'])
      sessionStorage.setItem("uid", response.headers['uid'])
      this.props.history.push("/home/listagem")
    }).catch((response) => {
      console.log(response)
    })
  }

  render() {
    return (
      <div className="login-page">
        <form onSubmit={this.login}>
          <div className="class-header">
            <img alt="Logo" className="logoHome" src={logoLogin} />
            <h2>Bem vindo ao empresas</h2>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">E-mail</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail" name="email" value={this.state.email} onChange={this.setUserFieldValue}/>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Senha</label>
            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Senha" name="password" value={this.state.password} onChange={this.setUserFieldValue}/>
          </div>
          <button type="submit" className="btn btn-secondary button-login">Entrar</button>
        </form>
      </div>
    )
  }
}

export default Login
