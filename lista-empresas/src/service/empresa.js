import axios from '../utils/axios'

export default class Home {

  static async login(user) {
    const response = await axios.post('users/auth/sign_in', user)
    return response;
  }

  static async getEmpresas() {
    const response = await axios.get('enterprises')
    return response.data;
  }

  static async getEmpresaById(id) {
    const response = await axios.get(`enterprises/${id}`)
    return response.data.enterprise;
  }

  static async getEmpresasByFields(fieldsValue) {
    const response = await axios.get('enterprises', {
      params: {
        enterprise_types: fieldsValue.tipo,
        name: fieldsValue.nome
      }
    })
    return response.data;
  }
}

export const baseImageUrl = 'http://54.94.179.135:8090'