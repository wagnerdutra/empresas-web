import axios from 'axios'

let instance;

instance = axios.create({
  baseURL: 'http://54.94.179.135:8090/api/v1/'
})

instance.interceptors.request.use((config) => {
    const token = sessionStorage.getItem("access-token")
    if (token !== null) {
      config.headers['access-token'] = sessionStorage.getItem("access-token")
      config.headers['client'] = sessionStorage.getItem("client")
      config.headers['uid'] = sessionStorage.getItem("uid")
    }
    return config
  },
  error => Promise.reject(error)
);

export default instance